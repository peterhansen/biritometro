/**
 * GameMIDlet.java
 * �2008 Nano Games.
 *
 * Created on Mar 20, 2008 3:18:41 PM.
 */

package core;

import br.com.nanogames.components.Drawable;
import br.com.nanogames.components.DrawableGroup;
import br.com.nanogames.components.ImageFont;
import br.com.nanogames.components.Label;
import br.com.nanogames.components.Pattern;
import br.com.nanogames.components.Updatable;
import br.com.nanogames.components.basic.BasicMenu;
import br.com.nanogames.components.basic.BasicSplashNano;
import br.com.nanogames.components.basic.BasicTextScreen;
import br.com.nanogames.components.userInterface.AppMIDlet;
import br.com.nanogames.components.userInterface.Menu;
import br.com.nanogames.components.userInterface.MenuListener;
import br.com.nanogames.components.userInterface.ScreenManager;
import javax.microedition.lcdui.Graphics;


/**
 * 
 * @author Peter
 */
public final class GameMIDlet extends AppMIDlet implements Constants, MenuListener {
	
	public static final byte GAME_MAX_FRAME_TIME = 110;
	
	private static final byte BACKGROUND_TYPE_SPECIFIC		= 0;
	private static final byte BACKGROUND_TYPE_BLACK			= 1;
	private static final byte BACKGROUND_TYPE_SOLID_COLOR	= 2;
	private static final byte BACKGROUND_TYPE_TABLE			= 3;
	private static final byte BACKGROUND_TYPE_GRAPH			= 4;
	private static final byte BACKGROUND_TYPE_NONE			= 5;
	
 	private static ImageFont[] FONTS = new ImageFont[ FONT_TYPES_TOTAL ];
	
	private static boolean lowMemory;
	
	private static TableScreen tableScreen;
	
	private static Graph graph;
	

	public GameMIDlet() {
		//#if SWITCH_SOFT_KEYS == "true"
//# 		super( VENDOR_SAGEM_GRADIENTE, GAME_MAX_FRAME_TIME );
		//#else
		super( -1, GAME_MAX_FRAME_TIME );
		//#endif
	}

	
	protected final void loadResources() throws Exception {		
		// carrega das fontes do jogo
		for ( byte i = 0; i < FONT_TYPES_TOTAL; ++i ) {
			FONTS[ i ] = ImageFont.createMultiSpacedFont( PATH_IMAGES + "font_" + i + ".png", PATH_IMAGES + "font_" + i + ".dat" );
		}
		FONTS[ FONT_INDEX_OUTLINE ].setCharOffset( FONT_OUTLINE_OFFSET );

		loadTexts( TEXT_TOTAL, PATH_IMAGES + "texts.dat" );
		
		setScreen( SCREEN_SPLASH_NANO );
	} // fim do m�todo loadResources()
	

	protected final int changeScreen( int screen ) throws Exception {
		final GameMIDlet midlet = ( GameMIDlet ) instance;
		
		Drawable nextScreen = null;

		final byte SOFT_KEY_REMOVE = -1;
		final byte SOFT_KEY_DONT_CHANGE = -2;

		byte bkgType = isLowMemory() ? BACKGROUND_TYPE_SOLID_COLOR : BACKGROUND_TYPE_SPECIFIC;

		byte indexSoftRight = SOFT_KEY_REMOVE;
		byte indexSoftLeft = SOFT_KEY_REMOVE;	
		
		switch ( screen ) {
			case SCREEN_SPLASH_NANO:
				bkgType = BACKGROUND_TYPE_NONE;
				//#if SCREEN_SIZE == "MEDIUM"
				nextScreen = new BasicSplashNano( SCREEN_SPLASH_GAME, BasicSplashNano.SCREEN_SIZE_MEDIUM, PATH_SPLASH, TEXT_SPLASH_NANO, -1 );				
				//#else
//# 				nextScreen = new BasicSplashNano( SCREEN_SPLASH_GAME, BasicSplashNano.SCREEN_SIZE_SMALL, PATH_SPLASH, TEXT_SPLASH_NANO, -1 );				
				//#endif
			break;
			
			case SCREEN_SPLASH_GAME:
				nextScreen = new SplashGame( getFont( FONT_INDEX_OUTLINE  ) );
				bkgType = BACKGROUND_TYPE_NONE;
			break;
			
			case SCREEN_DISCLAIMER:
				nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, getFont( FONT_INDEX_OUTLINE ), TEXT_DISCLAIMER, true ) {
					
					public final void update( int delta ) {
						final int previousTextOffset = label.getTextOffset();
						super.update( delta );
						if ( label.getTextOffset() != previousTextOffset && label.getTextOffset() == textLimitBottom ) {
							GameMIDlet.setScreen( SCREEN_MAIN_MENU );
						}
					}
					
					
					public final void keyPressed( int key ) {
						//#if DEBUG == "false"
//# 						if ( label.getTextOffset() <= -( label.getTextTotalHeight() >> 1 ) )
						//#endif
							GameMIDlet.setScreen( SCREEN_MAIN_MENU );
					}
					
				};
			break;
			
			case SCREEN_MAIN_MENU:
				if ( User.getFp_weight() == 0 ) {
					// primeiro birit�metro
					nextScreen = createBasicMenu( screen, new int[] {
						TEXT_NEW_GAME,
						TEXT_HELP,
						TEXT_CREDITS,
						TEXT_EXIT,

						//#if DEBUG == "true"
						TEXT_LOG_TITLE,
						//#endif							
						} );
				} else {
					nextScreen = createBasicMenu( screen, new int[] {
						TEXT_CONTINUE,
						TEXT_CHANGE_INFO,
						TEXT_NEW_GAME,
						TEXT_HELP,
						TEXT_CREDITS,
						TEXT_EXIT,

						//#if DEBUG == "true"
						TEXT_LOG_TITLE,
						//#endif							
						} );
				}

				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_EXIT;				
			break;
			
			case SCREEN_NEW_TABLE:
				tableScreen.reset();
			case SCREEN_CONTINUE_TABLE:
				tableScreen.resetHourLabels();
				nextScreen = tableScreen;
				
				bkgType = BACKGROUND_TYPE_TABLE;
			break;
			
			case SCREEN_GRAPH:
				graph = new Graph();
				nextScreen = graph;
				bkgType = BACKGROUND_TYPE_GRAPH;
			break;

			case SCREEN_HELP_MENU:
				nextScreen = createBasicMenu( screen, new int[] {
					TEXT_OBJECTIVES,
					TEXT_CONTROLS,
					TEXT_TIPS,
					TEXT_BACK,
					} );

				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;
			break;

			case SCREEN_HELP_OBJECTIVES:
			case SCREEN_HELP_CONTROLS:
			case SCREEN_HELP_TIPS:
            {
                String version = getAppProperty( "MIDlet-Version" );
                if ( version == null )
                    version = "1.0";
                
                version = "Vers�o " + version;
                
				nextScreen = new BasicTextScreen( SCREEN_HELP_MENU, getFont( FONT_INDEX_LIGHT ), getText( TEXT_HELP_OBJECTIVES + ( screen - SCREEN_HELP_OBJECTIVES ) ) + version, false, null );
				nextScreen.setSize( nextScreen.getWidth(), nextScreen.getHeight() - SOFTKEY_HEIGHT );
				( ( BasicTextScreen ) nextScreen ).setScrollPage( new ScrollBar() );
				( ( BasicTextScreen ) nextScreen ).setScrollFull( loadScrollFull() );
				
				indexSoftRight = TEXT_BACK;
            }
			break;
			
			case SCREEN_CREDITS:
				nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, getFont( FONT_INDEX_OUTLINE  ), TEXT_CREDITS_TEXT, true );
				nextScreen.setSize( nextScreen.getWidth(), nextScreen.getHeight() - SOFTKEY_HEIGHT );
				
				indexSoftRight = TEXT_BACK;				
			break;
			
			case SCREEN_LOADING_1:
				nextScreen = new LoadScreen( new LoadListener() {
						public final void load( LoadScreen loadScreen ) throws Exception {
							Thread.yield(); // permite que a outra thread execute, evitando problemas ao suspender em aparelhos Motorola
							
							Birita.loadImages();
							BiritaEditor.loadImages();
							ZoomEditor.loadImages();
							UserEditor.loadImages();

							tableScreen = new TableScreen();
							
							loadScreen.setActive( false );
							
							setScreen( SCREEN_DISCLAIMER );
						}
					}, getFont( FONT_INDEX_OUTLINE ), TEXT_LOADING );
				
				bkgType = BACKGROUND_TYPE_SPECIFIC;
			break;
			
			case SCREEN_USER_INFO:
				nextScreen = new UserEditor();
				bkgType = BACKGROUND_TYPE_NONE;
				
				indexSoftLeft = TEXT_OK;
				indexSoftRight = TEXT_BACK;
			break;
			
			case SCREEN_GRAPH_INFO:
				final StringBuffer buffer = new StringBuffer( getText( TEXT_GRAPH_INFO ) );
				buffer.append( graph.getDescription() );
				
				nextScreen = new BasicTextScreen( SCREEN_GRAPH, getFont( FONT_INDEX_LIGHT ), buffer.toString(), false );
				nextScreen.setSize( nextScreen.getWidth(), nextScreen.getHeight() - SOFTKEY_HEIGHT );
				( ( BasicTextScreen ) nextScreen ).setScrollPage( new ScrollBar() );
				( ( BasicTextScreen ) nextScreen ).setScrollFull( loadScrollFull() );
				
				bkgType = BACKGROUND_TYPE_SPECIFIC;
				indexSoftRight = TEXT_BACK;
			break;
			
			//#if DEBUG == "true"
			case SCREEN_ERROR_LOG:
				nextScreen = new BasicTextScreen( SCREEN_MAIN_MENU, getFont( FONT_INDEX_LIGHT ), texts[ TEXT_LOG_TEXT ], false );
				bkgType = BACKGROUND_TYPE_SOLID_COLOR;
				indexSoftRight = TEXT_BACK;
			break;
			//#endif
		} // fim switch ( screen )
		
		
		if ( indexSoftLeft != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_LEFT, indexSoftLeft );

		if ( indexSoftRight != SOFT_KEY_DONT_CHANGE )
			setSoftKeyLabel( ScreenManager.SOFT_KEY_RIGHT, indexSoftRight );	
		
		setBackground( bkgType );
		
		midlet.manager.setCurrentScreen( nextScreen );
		
		return screen;
	} // fim do m�todo changeScreen( int )


	private static final void setBackground( byte type ) {
		final GameMIDlet midlet = ( GameMIDlet ) instance;
		
		switch ( type ) {
			case BACKGROUND_TYPE_NONE:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( -1 );
			break;
			
			case BACKGROUND_TYPE_SPECIFIC:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( COLOR_BIRITA_EDITOR_BKG );
			break;
			
			case BACKGROUND_TYPE_BLACK:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( 0x000000 );
			break;
			
			case BACKGROUND_TYPE_TABLE:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( COLOR_CELL );
			break;
			
			case BACKGROUND_TYPE_GRAPH:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( COLOR_BACKGROUND );
			break;
			
			case BACKGROUND_TYPE_SOLID_COLOR:
			default:
				midlet.manager.setBackground( null, false );
				midlet.manager.setBackgroundColor( COLOR_TABLE_BLUE );
			break;
		}
	} // fim do m�todo setBackground( byte )
	
	
	private static final BasicMenu createBasicMenu( int index, int[] entries ) throws Exception {
		final BasicMenu menu = new BasicMenu( ( GameMIDlet ) instance, index, getFont( FONT_INDEX_OUTLINE  ), entries );		
		menu.setCursor( getCursor(), Menu.CURSOR_DRAW_BEFORE_MENU, Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_HCENTER ); 
		
		return menu;
	}	
	
	
	public static final boolean isLowMemory() {
		return lowMemory;
	}
	

	public final void onChoose( Menu menu, int id, int index ) {
		switch ( id ) {
			case SCREEN_MAIN_MENU:
				if ( User.getFp_weight() == 0 ) {
					switch ( index ) {
						case ENTRY_MENU_BIRITOMETRO:
							setScreen( SCREEN_USER_INFO );
						break;

						case ENTRY_MENU_HELP:
							setScreen( SCREEN_HELP_MENU );
						break;

						case ENTRY_MENU_CREDITS:
							setScreen( SCREEN_CREDITS );
						break;

						//#if DEBUG == "true"
						case ENTRY_MENU_ERROR_LOG:
							setScreen( SCREEN_ERROR_LOG );
						break;
						//#endif

						case ENTRY_MENU_EXIT:
							exit();
						break;
					} // fim switch ( index )
				} else {
					switch ( index ) {
						case ENTRY_CONTINUE_CONTINUE:
							setScreen( SCREEN_CONTINUE_TABLE ); 
						break;
						
						case ENTRY_CONTINUE_USER_INFO:
							setScreen( SCREEN_USER_INFO );
						break;

						case ENTRY_CONTINUE_BIRITOMETRO:
							setScreen( SCREEN_NEW_TABLE ); 
						break;

						case ENTRY_CONTINUE_HELP:
							setScreen( SCREEN_HELP_MENU );
						break;

						case ENTRY_CONTINUE_CREDITS:
							setScreen( SCREEN_CREDITS );
						break;

						//#if DEBUG == "true"
						case ENTRY_CONTINUE_ERROR_LOG:
							setScreen( SCREEN_ERROR_LOG );
						break;
						//#endif

						case ENTRY_CONTINUE_EXIT:
							exit();
						break;
					} // fim switch ( index )
				}
			break; // fim case SCREEN_MAIN_MENU
			
			case SCREEN_HELP_MENU:
				switch ( index ) {
					case ENTRY_HELP_MENU_OBJETIVES:
						setScreen( SCREEN_HELP_OBJECTIVES );
					break;
					
					case ENTRY_HELP_MENU_CONTROLS:
						setScreen( SCREEN_HELP_CONTROLS );
					break;
					
					case ENTRY_HELP_MENU_TIPS:
						setScreen( SCREEN_HELP_TIPS );
					break;
					
					case ENTRY_HELP_MENU_BACK:
						setScreen( SCREEN_MAIN_MENU );
					break;					
				}
			break;
			
			//#if DEMO == "true"
//# 			case SCREEN_BUY_GAME_EXIT:
//# 			case SCREEN_BUY_GAME_RETURN:
//# 				switch ( index )  {
//# 					case BasicConfirmScreen.INDEX_YES:
//# 						try {
//# 							String url = instance.getAppProperty( MIDLET_PROPERTY_URL_BUY_FULL );
//# 							
//# 							if ( url != null ) {
//# 								url += BUY_FULL_URL_VERSION + instance.getAppProperty( MIDLET_PROPERTY_MIDLET_VERSION ) +
//# 									   BUY_FULL_URL_CARRIER + instance.getAppProperty( MIDLET_PROPERTY_CARRIER );
//# 								
//# 								if ( instance.platformRequest( url ) ) {
//# 									exit();
//# 									return;
//# 								}
//# 							}
//# 						} catch ( Exception e ) {
							//#if DEBUG == "true"
//# 							e.printStackTrace();
							//#endif
//# 						}
//# 					break;
//# 				} // fim switch ( index )
//# 				
//# 				setScreen( id == SCREEN_BUY_GAME_EXIT ? SCREEN_BUY_ALTERNATIVE_EXIT : SCREEN_BUY_ALTERNATIVE_RETURN );
//# 			break;
//# 			
//# 			case SCREEN_BUY_ALTERNATIVE_EXIT:
//# 				exit();
//# 			return;
//# 			
//# 			case SCREEN_BUY_ALTERNATIVE_RETURN:
//# 			case SCREEN_PLAYS_REMAINING:
//# 				setScreen( SCREEN_MAIN_MENU );
//# 			break;
//# 			
			//#endif
			
		} // fim switch ( id )		
	}
	
	
	public final void onItemChanged( Menu menu, int id, int index ) {
		if ( menu != null && menu.getCursor() != null && menu.getDrawable( index ) != null ) {
			menu.getCursor().setSize( menu.getWidth() - 2, menu.getDrawable( index ).getHeight() );
			menu.getCursor().defineReferencePixel( Drawable.ANCHOR_VCENTER | Drawable.ANCHOR_HCENTER );
		}
	}
	
	
	public static final Drawable getCursor() {
		return new Pattern( COLOR_BIRITA_ENTRY_SELECTED );
	}
	
	
	/**
	 * Define uma soft key a partir de um texto.
	 * 
	 * @param softKey �ndice da soft key, conforme definido em <code>ScreenManager</code>. Valores negativos indicam
	 * para remover a soft key atual, caso exista.
	 * @param textIndex indice do texto.
	 */
	public static final void setSoftKeyLabel( byte softKey, int textIndex ) {
		if ( textIndex < 0 ) {
			ScreenManager.getInstance().setSoftKey( softKey, null );
		} else {
			ScreenManager.getInstance().setSoftKey( softKey, loadSoftKeyGroup( softKey, textIndex ) );
		}
	}
	
	
	public static final DrawableGroup loadSoftKeyGroup( byte softKey, int textIndex ) {
		try {
			final DrawableGroup group = new DrawableGroup( 2 );

			final Pattern bkg = new Pattern( COLOR_BIRITA_EDITOR_BKG );
			bkg.setSize( SOFTKEY_WIDTH, SOFTKEY_HEIGHT );

			switch ( softKey ) {
				case ScreenManager.SOFT_KEY_LEFT:
					bkg.setPosition( 1, 0 );

				case ScreenManager.SOFT_KEY_RIGHT:
					group.setSize( SOFTKEY_WIDTH + 1, SOFTKEY_HEIGHT + 1 );
				break;

				case ScreenManager.SOFT_KEY_MID:
					group.setSize( SOFTKEY_WIDTH, SOFTKEY_HEIGHT + 1 );
				break;
			}
			group.insertDrawable( bkg );

			final Label label = new Label( getFont( FONT_INDEX_LIGHT ), getText( textIndex ) );
			label.setPosition( ( group.getWidth() - label.getWidth() ) >> 1, ( group.getHeight() - label.getHeight() ) >> 1 );
			group.insertDrawable( label );

			return group;
		} catch ( Exception e ) {
			//#if DEBUG == "true"
			e.printStackTrace();
			//#endif
			
			return null;
		}		
	}
	
	
	private static final Drawable loadScrollFull() {
		final DrawableRect border = new DrawableRect( COLOR_BIRITA_ENTRY_SELECTED );
		border.setSize( SCROLL_WIDTH, 0 );

		return border;
	}
	
	
	public static final ImageFont getFont( int index ) {
		return FONTS[ index ];
	}
	

	private static final class ScrollBar extends DrawableGroup {
		
		private final Pattern pattern;
		
		private ScrollBar() {
			super( 1 );
			
			pattern = new Pattern( COLOR_BACKGROUND );
			pattern.setSize( SCROLL_WIDTH - 2, 0 );
			pattern.setPosition( 1, 0 );
			insertDrawable( pattern );
			
			setSize( SCROLL_WIDTH, 0 );
		}
		
		
		public final void setSize( int width, int height ) {
			super.setSize( width, height );
			
			pattern.setSize( pattern.getWidth(), height );
		}
	}
	
	
	// <editor-fold desc="CLASSE INTERNA LOADSCREEN" defaultstate="collapsed">
	
	private static interface LoadListener {
		public void load( final LoadScreen loadScreen ) throws Exception;
	}
	
	
	private static final class LoadScreen extends Label implements Updatable {

		/** Intervalo de atualiza��o do texto. */
		private static final short CHANGE_TEXT_INTERVAL = 330;

		private long lastUpdateTime;

		private static final byte MAX_DOTS = 4;
		
		private static final byte MAX_DOTS_MODULE = MAX_DOTS -1 ;

		private byte dots;

		private Thread loadThread;

		private final LoadListener listener;

		private boolean painted;
		
		private final byte previousScreen;
		
		private boolean active = true;


		/**
		 * 
		 * @param listener
		 * @param id
		 * @param font
		 * @param loadingTextIndex
		 * @throws java.lang.Exception
		 */
		private LoadScreen( LoadListener listener, ImageFont font, int loadingTextIndex ) throws Exception {
			super( font, AppMIDlet.getText( loadingTextIndex ) );
			
			previousScreen = currentScreen;
			
			this.listener = listener;

			setPosition( ( ScreenManager.SCREEN_WIDTH - size.x ) >> 1, ( ScreenManager.SCREEN_HEIGHT - size.y ) >> 1 );
			
			ScreenManager.setKeyListener( null );
			ScreenManager.setPointerListener( null );
		}


		public final void update( int delta ) {
			final long interval = System.currentTimeMillis() - lastUpdateTime;

			if ( interval >= CHANGE_TEXT_INTERVAL ) {
				// os recursos do jogo s�o carregados aqui para evitar sobrecarga do m�todo loadResources, o que
				// leva a uma demora excessiva para abrir o jogo em alguns aparelhos
				if ( loadThread == null ) {
					// s� inicia a thread quando a tela atual for a ativa, para evitar poss�veis atrasos em transi��es e
					// garantir que novos recursos s� ser�o carregados quando a tela anterior puder ser desalocada por completo
					if ( ScreenManager.getInstance().getCurrentScreen() == this && painted ) {
						ScreenManager.setKeyListener( null );
						ScreenManager.setPointerListener( null );
						
						final LoadScreen loadScreen = this;
						
						loadThread = new Thread() {
							public final void run() {
								try {
									System.gc();
									listener.load( loadScreen );
								} catch ( Throwable e ) {
									//#if DEBUG == "true"
									e.printStackTrace();
									texts[ TEXT_LOG_TEXT ] += e.getMessage().toUpperCase();

									setScreen( SCREEN_ERROR_LOG );
									e.printStackTrace();
									//#else
//# 										// volta � tela anterior
//# 										setScreen( previousScreen );
									//#endif
								}
							} // fim do m�todo run()
						};
						loadThread.start();
					}
				} else if ( active ) {
					lastUpdateTime = System.currentTimeMillis();

					dots = ( byte ) ( ( dots + 1 ) & MAX_DOTS_MODULE );
					String temp = GameMIDlet.getText( TEXT_LOADING );
					for ( byte i = 0; i < dots; ++i )
						temp += '.';

					setText( temp );

					try {
						// permite que a thread de carregamento dos recursos continue sua execu��o
						Thread.sleep( CHANGE_TEXT_INTERVAL );
					} catch ( Exception e ) {
						//#if DEBUG == "true"
						e.printStackTrace();
						//#endif
					}					
				}
			}
		} // fim do m�todo update( int )
		
		
		public final void paint( Graphics g ) {
			super.paint( g );
			
			painted = true;
		}
		
		
		private final void setActive( boolean a ) {
			active = a;
		}

	} // fim da classe interna LoadScreen


	// </editor-fold>


}
